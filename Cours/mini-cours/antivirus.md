# Mini-cours sur les antivirus 

# Histoire des Virus

Le premier programme malveillant a fait son apparition sur les ordinateurs en 1974 sous le nom de virus «Rabbit» ("lapin"), ainsi nommé en raison de sa vitesse. Le virus Rabbit a été conçu non seulement pour se reproduire afin de se répandre, mais aussi pour bloquer l'ordinateur infecté en utilisant des ressources informatiques précieuses comme la RAM. Le virus ralentissait de façon exponentielle les ordinateurs, puis provoquait finalement une panne en consommant tout leur puissance de traitement.

Jusqu'en 1987, lorsque le premier antivirus fût créé, il n'y avait pas beaucoup de mouvements dans l'univers des virus.
 Le plus gros virus et le plus dévastateur jusqu'alors était le «virus de Jérusalem», qui a pour la première fois été détecté dans la ville dont il porte le nom. Le virus de Jérusalem, lorsqu'il infectait un PC, détruisait tous les fichiers .exe (fichier exécutable, le type de fichier typique pour un programme) chaque vendredi 13. La première fois que cela s'est produit, en mai 1988, le virus a provoqué une «épidémie mondiale».

La même année, Bernd Fix réussit la première suppression d'un virus informatique.

À la fin de l'année 1987, il y eût un gros changement dans l'industrie des antivirus, avec la fondation de la société McAfee désormais célèbre dans le monde entier (détenue à présent par Intel) et la création d'un premier ensemble de logiciels antivirus. G Data Software aurait été le premier à commercialiser cette année-là avec 'Ultimate Virus Killer 2000', suivi peu après par 'Virus Scan' de McAfee.

## Qu'est ce qu'un antivirus ? 

Programme qui à pour but de protéger l'intégrité de votre système et de vos données par la détection de "virus" ou des malware.

![diffvirusmalware](./img/antivirus/diff-virusmalware.png)

Pour cela il est effectue une floppée d'actions qui vont permettre d'éviter que ton PC flambe du jour au lendemain. 

L'antivirus : 

- Il doit d'abord s'assurer que c'est un vrai danger.
- Il doit vérifier que ses agissements sont bien anormaux et préjudiciables à l'intégrité du Système.
- S'il s'avère que c'est bien un programme nuisible, il doit décider quelle est la meilleure action à envisager en fonction des dégâts causés.
- Si le code malveillant détecté est complexe, semble très dangereux et / ou inconnu et qu'il ne sait pas comment l'éradiquer, il le déplace dans une zone de Quarantaine.

## Quels sont les outils d'un antivirus ? 

- La détection par la signature : \
Simple, t'as une base de donnée avec la signature de ton programme. Si la signature correspond pas, ça le dégage.

- La détection par le comportement : \
Si le comportement du processus lui plait pas, il va essayer de le réparer, le supprimer ou s'il a un doute, le foutre en quarantaine.  

- La détection par le contrôle de l'intégrité: \
L'antivirus va vérifier si le programme a été modifié depuis son installation. 

- La détection par l'analyse heuristique: \
L'antivirus va executer le programme dans une zone isolée afin de vérifier son comportement.
Le programme sera donc analysé dans toutes ses coutures ce qui permettra (si c'est un logiciel malveillant ) de récupérer les infos liées a lui et de le supprimer, même si celui-ci est inconnu. 
